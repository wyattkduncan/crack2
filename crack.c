#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
/*int tryguess(char *hash, char *guess)
{
    char *e = guess;
    // Hash the guess using MD5
    while (*e && *e != '\r' && *e != '\n' )
        e++;
    char* hashedGuess = md5(guess, e- guess);
    
    // Compare the two hashes
    if (strcmp(guess, hashedGuess))
    {
        return 1;
    }
    else {
    return 0;
    }
    // Free any malloc'd memory
    free(hashedGuess);
}*/



int tryguess(char *hash, char *guess)
{
    int valid = 0;
    char* hashes;
    hashes =  md5(guess, strlen(guess));
    if (strcmp(hashes, hash) == 0) {
        valid = 1;
    }
    free(hashes);
    return valid;
}



// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    // Get size of file
    struct stat info;
    stat(filename, &info);
    int file_length = info.st_size;
    
    printf("Size: %d\n", file_length);
    
    // Allocate space for the entire file
    char *chunk = (char *)malloc(file_length+1);
    
    // Read in entire file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("Can't open file");
        exit(1);
    }
    fread(chunk, 1, file_length, f);
    fclose(f);
    
    // Add null character to end
    chunk[file_length] = '\0';
    
    // Change newlines to nulls, counting as we go
    int lines = 0;
    for (int i = 0; i < file_length; i++)
    {
        if (chunk[i] == '\n') 
        {
            chunk[i] = '\0';
            lines++;
        }
    }
    
    // If the file didn't have a final terminating newline,
    // it won't be counted. Let's check for that situation
    // and adjust the linecount accordingly. Since we've already
    // changed newlines to null, all we need to do is look
    // for a null character at the end of the chunk.
    if (chunk[file_length-1] != '\0') lines++;
    
    // Allocate space for an array of pointers
    char **strings = malloc((lines+1) * sizeof(char *));
    
    // Loop through chunk, storing pointers to each string
    int string_idx = 0;
    for (int i = 0; i < file_length; i += strlen(chunk+i) + 1)
    {
        strings[string_idx] = chunk+i;
        string_idx++;
    }
    
    // Store terminating NULL
    strings[lines] = NULL;
    
    // Print it all out
    int x = 0;
    while (strings[x] != NULL)
    {
        //printf("%d: %s\n", x, strings[x]);
        x++;
    }
    return strings;
    free(strings);
    free(chunk);
    
    
    
    
    
    /*//get size of file
  struct stat info;
  stat(filename, &info);
  int file_length = info.st_size;
  
  printf("Size: %d\n", file_length);
  
  //Allocate file space in memory
  char *chunk  = (char *)malloc(file_length+1);
  
  //read in entire file
  FILE *f = fopen(filename, "r");
  if (!f)
  {
     perror("Can't open file");
     exit(1);
  }
  
  fread(chunk, 1, file_length, f);
  fclose(f);
  
  //Add null char to end
  chunk[file_length] = 0;
  
  //change new lines to nulls
  int lines = 0;
  for(int i = 0; i < file_length; i++)
  {
     if(chunk[i] == '\n') 
    {
    chunk[i] = '\0';
    lines++;
    }
  }
  
  //Allocate space for array of pointers
  char **strings = malloc((lines+1) * sizeof(char *));
  
  //loop through chunk, storing pointers to each strings
  int string_index = 0;
  for(int i = 0; i < file_length; i += strlen(&chunk[i]) + 1)
  {
   strings[string_index] = chunk+1;
   string_index++;
  }
  
  //store terminating null
  strings[string_index] = NULL;
  
  //print iut all out
  int x = 0;
  while (strings[x] != NULL)
  {
  printf("%d: %s\n", x, strings[x]);
  x++;
  
  }
  return strings;
  free(strings);
    free(chunk);*/
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    
    printf("The cracked passwords are: \n");
    //int sizeH = sizeof(hashes);
    //printf("the size of the hash array is: %d \n", sizeH);
    
    
    /*int count = 0;
    printf("0: \n");
    while (hashes[count] != NULL){
        printf("1: \n");
        for (int i=0; dict[i] != NULL; i++) {
            printf("2: \n");
            if (tryguess(hashes[count], dict[i])){
                printf("3: \n");
                count++;
                printf("%s\n", dict[i]);
                break;
            }
        }
    }*/
    
    
    
     int w = 0;
    while (hashes[w] != NULL) {
        printf("%s  :", hashes[w]);
        for (int i = 0 ; dict[i] != NULL ; i++) {
            if (tryguess(hashes[w], dict[i])) {
                //printf("entered");
                w++;
                printf("%s\n",dict[i]);
                break;
            }
        }
    }
    
    
    free(*hashes);
    free(*dict);
    free(hashes);
    free(dict);
}

